#!/usr/bin/perl -w

# Copyright (C) 2006, 2007 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;
use CGI qw/:cgi/;
use CGI::Carp;
my $query = new CGI;

if ($query->param('text')) {
    print "Content-Type: text/plain\n\n";
} else {
    print "Content-Type: text/html\n\n";

    print <<EOT;
<title>dak ls aka madison</title>
<head>
EOT

    print <<EOT;
<script type="text/javascript" language="JavaScript">
function objectOver(x){
  x.style.color="blue";
}
function objectOut(x){
  x.style.color="black";
}
function toggle(x) {
  var oContent=document.getElementById(x) ;
  if (oContent.style.display=="block") {
    oContent.style.display="none";
  } else {
    oContent.style.display="block";
  }
}
</script>
EOT

    print <<EOT;
</head>
<body>
EOT
}

my $token_re = "[a-z0-9][a-z0-9.+-]+";
my $token = "^($token_re)\$";
my $token_list = "^((?:$token_re\[ ,]?)+)\$";
my $opt = "";

if ($query->param('a') and $query->param('a') =~ /$token_list/io) {
    $opt .= " -a'$1'";
}
if ($query->param('b') and $query->param('b') =~ /$token/io) {
    $opt .= " -b'$1'";
}
if ($query->param('c') and $query->param('c') =~ /$token_list/io) {
    $opt .= " -c'$1'";
}
if ($query->param('g')) {
    $opt .= " -g";
}
if ($query->param('G')) {
    $opt .= " -G";
}
# no -r so far since it can take quite long to run
if ($query->param('s') and $query->param('s') =~ /$token_list/io) {
    $opt .= " -s'$1'";
}
if ($query->param('S')) {
    $opt .= " -S";
}

if ($query->param('package') and $query->param('package') =~ /$token_list/io) {
    if (!$query->param('text')) {
        print "<h2>dak ls $1</h2>\n";
        print("<!-- dak ls $opt $1 -->\n");
        print("<pre>\n");
    }
    $| = 1;
    #system("dak ls $opt $1 2>&1");
    my $output = `dak ls $opt $1 2>&1`;
    if ($output =~ /FATAL:.*database.*does not exist/) {
	print "Database down for daily maintenance, please try again in a few minutes.\n";
    }
    else {
	print "$output\n";
    }
    if (!$query->param('text')) {
        print "</pre>\n";
    }
} elsif (!$query->param('text')) {
    print "<h2>dak ls</h2>\n";
}

exit if ($query->param('text'));

print "<p>
<form action=\"madison.cgi\" method=\"get\">
<input type=\"text\" name=\"package\" value=\"" . ($query->param('package') || "") . "\">
<input type=\"submit\" value=\"Query\">
<br>
<small> Show:
<span onmouseover='javascript:objectOver(this)' onmouseout='javascript:objectOut(this)' onclick='javascript:toggle(\"config\")'>More options</span>,
<span onmouseover='javascript:objectOver(this)' onmouseout='javascript:objectOut(this)' onclick='javascript:toggle(\"help\")'>help</span>
</small><br>
<span id=\"config\" style=\"display:". ($opt ? "block" : "none") ."\">
<table border=\"0\">
<tr><td>architecture</td><td><input type=\"text\" name=\"a\" value=\"" . ($query->param('a') || "") . "\"></td></tr>
<tr><td>binary-type</td><td><input type=\"text\" name=\"b\" value=\"" . ($query->param('b') || "") . "\"></td></tr>
<tr><td>component</td><td><input type=\"text\" name=\"c\" value=\"" . ($query->param('c') || "") . "\"></td></tr>
<tr><td>greaterorequal</td><td><input type=\"checkbox\" name=\"g\"" . ($query->param('g') ? " checked" : "") ."></td></tr>
<tr><td>greaterthan</td><td><input type=\"checkbox\" name=\"G\"" . ($query->param('G') ? " checked" : "") . "></td></tr>
<tr><td>suite</td><td><input type=\"text\" name=\"s\" value=\"" . ($query->param('s') || "") . "\"></td></tr>
<tr><td>source-and-binary</td><td><input type=\"checkbox\" name=\"S\"" . ($query->param('S') ? " checked" : "") ."></td></tr>
<tr><td>text-only</td><td><input type=\"checkbox\" name=\"text\"></td></tr>
</table>
</span>
</form>

<p>
<span id=\"help\" style=\"display:none\">
<pre>
Usage: dak ls [OPTION] PACKAGE[...]
Display information about PACKAGE(s).

  -a, --architecture=ARCH    only show info for ARCH(s)
  -b, --binary-type=TYPE     only show info for binary TYPE
  -c, --component=COMPONENT  only show info for COMPONENT(s)
  -g, --greaterorequal       show buildd 'dep-wait pkg &gt;= {highest version}' info
  -G, --greaterthan          show buildd 'dep-wait pkg &gt;&gt; {highest version}' info
  -h, --help                 show this help and exit
  -r, --regex                treat PACKAGE as a regex <i>[not supported in madison.cgi]</i>
  -s, --suite=SUITE          only show info for this suite
  -S, --source-and-binary    show info for the binary children of source pkgs

ARCH, COMPONENT and SUITE can be comma (or space) separated lists, e.g.
    --architecture=amd64,i386
</pre>
</span>
";

# vim:sw=4:et:
