<?php
define('CACHE_DIR', '/srv/bugs.qa.debian.org/data');
define('COMMENTS_DIR', CACHE_DIR.'/comments');
define('BUGS_DIR', CACHE_DIR.'/bugs');
define('BUGS_ETC_DIR', '/srv/bugs.debian.org/etc');
define('FTP_DIR', '/srv/ftp.debian.org/ftp/dists');
define('DB_DIR', CACHE_DIR.'/db');
define('GROUP_DIR', CACHE_DIR.'/lists');
define('HINTS_DIR', CACHE_DIR.'/hints');
define('DELAYED_DIR', CACHE_DIR.'/delayed');
define('CLAIMS_CACHE', CACHE_DIR.'/bugsquash-usertags');
?>
