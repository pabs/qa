#!/bin/sh -e

ROOT="/srv/bugs.qa.debian.org"
HISTPATH="$ROOT/data/history/"
BUGS="$ROOT/data/db/bugs.db"
CMP="$ROOT/bin/diffbugs.php"

cd $HISTPATH
FILES=`ls -1 bugs-*.db | tail -2`
set $FILES

rm -f $BUGS
ln -s $HISTPATH/$2 $BUGS

CMP_HTML="`basename $2 .db`.html"
php4 $CMP $1 $2 > $CMP_HTML

#INDEX="$ROOT/wml/bugs-diff.html"
#FILES=`ls -1 bugs-*.html | tail -4 | tac`
#cat $FILES > $INDEX
