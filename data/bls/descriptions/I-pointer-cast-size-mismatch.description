## Match: regexp:[^:]*:[0-9]\+\(:[0-9]\+\)\?: warning:\/ cast to pointer from integer of different size
## Extract: simple 0 2 ':'
## Since: 2
The build log contains the term
<pre>
warning: cast to pointer from integer of different size
</pre>
<p>
This means a pointer is stored in an integer variable losing information.
</p>
<ul>
<li>If the pointer was only used to store an integer, nothing bad happens.
To silence this compiler warning, one can first cast it to <tt>intptr_t</tt>
before casting it to a pointer.
<li>If the pointer is stored as an integer and this later restored into
an pointer, the upper bits of the pointer got discarded, causing the program
work in 32 bit architectures but often fail on 64 bit architectures.
</ul>
<p>
If this compiler warning is emitted for the same line as a previous implicit
declaration warning, the tag <a href="E-pointer-trouble-at-implicit.html">E-pointer-trouble-at-implicit</a> is generated instead.
</p>
